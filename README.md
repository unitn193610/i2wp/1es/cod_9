# HOMEWORK 3

EXTENSION

TinyHttd can only serve files. A cool extension would be to enable file listing for directories. This feature is available in most http servers as "DirectoryIndex".
Extend our implementation to return a list of files (in html) if the client request a folder, for example:
GET /papers/
